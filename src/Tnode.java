import java.util.*;
import java.util.regex.Pattern;

public class Tnode  {

   //Solution from https://www.baeldung.com/java-check-string-number
   private static final Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");

   private static final List<String> binaryOperators = Arrays.asList("+", "-", "/", "*");

   private String name;
   private Tnode firstChild = null;
   private Tnode nextSibling = null;

   public Tnode(String name) {
      if (name == null) {
         throw new NullPointerException("Given input was null.");
      }

      setName(name);
   }

   public static void main (String[] param) {
//      String rpn = "1 2 +";
//      String rpn = "2 xx";
//      String rpn = "2 3";
//      String rpn = "2 -";
//      String rpn = "+";
//      String rpn = "2";
      String rpn = "2 1 - 4 * 6 3 / +";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);

      System.out.print("Postorder: ");
      res.postorder();
      System.out.println();

      System.out.print("Preorder: ");
      res.preorder();
      System.out.println();
   }

   @Override
   public Object clone() {

      Tnode clone = new Tnode(name);

      if (firstChild != null) {
         clone.firstChild = (Tnode) firstChild.clone();
      }

      if (nextSibling != null) {
         clone.nextSibling = (Tnode) nextSibling.clone();
      }

      return clone;
   }

   private void setName(String name) {
      this.name = name;
   }

   private String getName(){
      return name;
   }

   private boolean hasFirstChild() {
      return !(firstChild == null);
   }

   private void setFirstChild(Tnode firstChild) {
      this.firstChild = firstChild;
   }

   private Tnode getFirstChild() {
      return firstChild;
   }

   private boolean hasNextSibling() {
      return !(nextSibling == null);
   }

   private void setNextSibling(Tnode nextSibling) {
      this.nextSibling = nextSibling;
   }

   private Tnode getNextSibling() {
      return nextSibling;
   }

   //Solution from https://www.baeldung.com/java-check-string-number
   private static boolean isNumeric(String strNum) {
      if (strNum == null) {
         return false;
      }

      return pattern.matcher(strNum).matches();
   }

   private void printTNodeData() {
      String firstChildName = getFirstChild() == null ? "NULL" : getFirstChild().getName();
      String nextSiblingName = getNextSibling() == null ? "NULL" : getNextSibling().getName();

      System.out.print (String.format(
              "Name: %s : FirstChild: %s : NextSibling %s", getName(), firstChildName, nextSiblingName));
   }

   //Solution from https://enos.itcollege.ee/~jpoial/algoritmid/puud.html
   private void processTreeNode() {
      System.out.print (getName() + "  ");
   }

   //Solution from https://enos.itcollege.ee/~jpoial/algoritmid/puud.html
   private void postorder() {
      Tnode child = getFirstChild();

      while (child != null) {
         child.postorder();
         child = child.getNextSibling();
      }

      processTreeNode();
   }

   //Solution from https://enos.itcollege.ee/~jpoial/algoritmid/puud.html
   private void preorder() {
      processTreeNode();

      Tnode child = getFirstChild();

      while (child != null) {
         child.preorder();
         child = child.getNextSibling();
      }
   }

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();

      b.append(getName());

      if (hasFirstChild()) {
         b.append(String.format("(%s)", getFirstChild().toString()));
      }

      if (hasNextSibling()) {
         b.append(",");
         b.append(String.format("%s", getNextSibling().toString()));
      }

      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      Tnode firstTNode;
      Tnode thirdTNode;
      Tnode secondTNode;

      if (pol == null) {
         throw new NullPointerException("Given input was null.");
      }

      if (pol.trim().equals("")) {
         throw new IllegalArgumentException("Given input was empty.");
      }

      String[] polAsArr = pol.trim().split(" ");

      LinkedList<Tnode> tNodes = new LinkedList<>();

      for (String tNodeName : polAsArr) {

         if (isNumeric(tNodeName)) {
            tNodes.addFirst(new Tnode(tNodeName));
            continue;
         }

         if (tNodeName.equals("DUP")) {
            try {
               firstTNode = tNodes.getFirst();

            } catch (NoSuchElementException e) {
               throw new NoSuchElementException(String.format(
                       "Input was \"%s\". Not enough elements to use for operator \"%s\"", pol, tNodeName));
            }

            tNodes.addFirst((Tnode) firstTNode.clone());
            continue;
         }

         if (tNodeName.equals("SWAP")) {
            try {
               firstTNode = tNodes.removeFirst();
               secondTNode = tNodes.removeFirst();

            } catch (NoSuchElementException e) {
               throw new NoSuchElementException(String.format(
                       "Input was \"%s\". Not enough elements to use for operator \"%s\"", pol, tNodeName));
            }

            tNodes.addFirst(firstTNode);
            tNodes.addFirst(secondTNode);

            continue;
         }

         if (tNodeName.equals("ROT")) {
            try {
               firstTNode = tNodes.removeFirst();
               secondTNode = tNodes.removeFirst();
               thirdTNode = tNodes.removeFirst();

            } catch (NoSuchElementException e) {
               throw new NoSuchElementException(String.format(
                       "Input was \"%s\". Not enough elements to use for operator \"%s\"", pol, tNodeName));
            }

            tNodes.addFirst(secondTNode);
            tNodes.addFirst(firstTNode);
            tNodes.addFirst(thirdTNode);

            continue;
         }


         if (!binaryOperators.contains(tNodeName)) {
            throw new IllegalArgumentException(String.format(
                    "Input was \"%s\" where operator \"%s\" is unknown.", pol, tNodeName));
         }

         Tnode firstChild;
         Tnode nextSibling;

         Tnode operatorTNode = new Tnode(tNodeName);

         try {
            nextSibling = tNodes.removeFirst();
            firstChild = tNodes.removeFirst();

         } catch (NoSuchElementException e) {
            throw new NoSuchElementException(String.format(
                    "Input was \"%s\". Not enough elements to use for operator \"%s\"", pol, tNodeName));
         }

         operatorTNode.setFirstChild(firstChild);
         firstChild.setNextSibling(nextSibling);

         tNodes.addFirst(operatorTNode);
      }

      if (tNodes.size() > 1) {
         throw new IllegalArgumentException(String.format(
                 "Input was \"%s\". Not possible to create root element. Too many numbers.", pol));
      }

      return tNodes.removeLast();
   }
}

